public class SemiPreciousStone extends Stone {
    private final static double CARAT_PRICE = 1.0;

    public SemiPreciousStone(double carat, double transparency) {
        super(carat, transparency);
    }

    @Override
    public double getPrice() {
        return this.getCarat() * CARAT_PRICE;
    }

    @Override
    public String toString() {
        return "Semi Precious " + super.toString();
    }
}
