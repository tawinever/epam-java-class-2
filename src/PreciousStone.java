public class PreciousStone extends Stone {
    private final static double CARAT_PRICE = 2.0;

    public PreciousStone(double carat, double transparency) {
        super(carat, transparency);
    }

    @Override
    public double getPrice() {
        return this.getCarat() * CARAT_PRICE;
    }

    @Override
    public String toString() {
        return "Precious " + super.toString();
    }
}
