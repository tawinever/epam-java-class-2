import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Stone> stones = new ArrayList<>();
        stones.add(new SemiPreciousStone(10, 0.5));
        stones.add(new PreciousStone(15, 0.2));

        Necklace necklace = new Necklace(stones);

        necklace.getTotalCarat();
        necklace.getTotalPrice();

        necklace.sortStonesByPrice();

        necklace.findAllStonesByTransparencyInInterval(0.3, 0.6);
    }
}
