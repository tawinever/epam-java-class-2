public abstract class Stone {
    private double carat;
    private double transparency;

    public double getCarat() {
        return carat;
    }

    protected void setCarat(double carat) {
        this.carat = carat;
    }

    public double getTransparency() {
        return transparency;
    }

    protected void setTransparency(double transparency) {
        this.transparency = transparency;
    }


    public Stone(double carat, double transparency) {
        this.carat = carat;
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return "Stone{" +
                "carat=" + carat +
                ", transparency=" + transparency +
                '}';
    }

    public abstract double getPrice();

}
