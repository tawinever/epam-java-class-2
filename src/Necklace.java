import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Necklace {
    private List<Stone> stones = new ArrayList<>();

    public Necklace(List<Stone> stones) {
        this.stones = stones;
        System.out.println("Created new necklace with stones: " + stones);
    }

    public double getTotalCarat() {
        double totalCarat = stones.stream().mapToDouble(Stone::getCarat).sum();
        System.out.println("Total carat is " + totalCarat);
        return totalCarat;
    }

    public double getTotalPrice() {
        double totalPrice = stones.stream().mapToDouble(Stone::getPrice).sum();
        System.out.println("Total price is " + totalPrice);
        return totalPrice;
    }

    public void sortStonesByPrice() {
        stones.sort(new Comparator<Stone>() {
            @Override
            public int compare(Stone o1, Stone o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        });
        System.out.println("Stones were sorted by price");
        System.out.println(stones);
    }

    public List<Stone> findAllStonesByTransparencyInInterval(double minimumTransparency, double maximumTransparency) {
        List<Stone> result = stones.stream()
                .filter(o -> o.getTransparency() >= minimumTransparency)
                .filter(o -> o.getTransparency() <= maximumTransparency)
                .collect(Collectors.toList());
        System.out.println("The number of stones found in searched interval is " + result.size());
        System.out.println(result);
        return result;
    }



}
